from random import randint

from const import *
from world import world



class Entity:
    def __init__(self):
        self.color = (0, 0, 0)  # RGB format
        self.alive = False
        self.health = 0
        self.energy = None  # If entity is Food = valuation, If entity is Bot = number
        self.pos = [0, 0]
        self.type = None  # Food or Bot
        self.dna = []

    def action(self):
        if self.type == 'Bot':
            for dna in self.dna:
                if dna <= 10:  # Unconditional movement
                    dice = randint(1, 4)
                    if dice == 1 and self.pos[0] > 0:
                        self.pos[0] -= STEP_SIZE
                    if dice == 2 and self.pos[0] < WORLD_SIZE[0]:
                        self.pos[0] += STEP_SIZE
                    if dice == 3 and self.pos[1] > 0:
                        self.pos[1] -= STEP_SIZE
                    if dice == 4 and self.pos[1] < WORLD_SIZE[1]:
                        self.pos[1] += STEP_SIZE

                if dna in range(20, 30):  # Eat if can see
                    for food in world.objects:
                        if self.pos[0] <= food.pos[0] <= self.pos[0] + 10 and self.pos[1] <= \
                                food.pos[1] <= self.pos[1] + 10 and food.type != self.type:
                            food.alive = False
                            self.energy += food.energy
                            self.health = 100

        if self.type == 'Food':
            dice = randint(1, 4)
            if dice == 1 and self.pos[0] > 0:
                self.pos[0] -= 1
            if dice == 2 and self.pos[0] < WORLD_SIZE[0]:
                self.pos[0] += 1
            if dice == 3 and self.pos[1] > 0:
                self.pos[1] -= 1
            if dice == 4 and self.pos[1] < WORLD_SIZE[1]:
                self.pos[1] += 1

    def reborn(self):
        if self.energy >= 100:
            dice = randint(1, 4)
            if dice in range(1, 3):
                world.objects.append(Bot(dna=self.dna, color=self.color))
            else:
                self.mutation()
            self.energy = 0

    def mutation(self):
        dna = self.dna
        color = (randint(100, 255), randint(100, 255), randint(100, 255))
        dna[randint(0, 31)] = randint(1, 64)
        world.objects.append(Bot(dna=dna, color=color))

    def is_alive(self):
        if self.health <= 0:
            self.alive = False
        return self.alive

    def activate(self):
        if self.is_alive():
            self.reborn()
            self.action()
            self.health -= 1
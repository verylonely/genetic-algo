import entities
import pygame
from random import randint

from Bot import Bot
from Food import Food
from const import *
from world import world

def clear_dead():
    for e in world.objects:
        if not e.alive:
            world.objects.remove(e)


def food_respawn():
    counter = 0
    for e in world.objects:
        if e.type == 'Food':
            counter += 1
    if counter < FOOD_LIMIT:
        world.objects.append(Food())

pygame.init()

print('Version:', VERSION)

window = pygame.display.set_mode(WORLD_SIZE)

for i in range(START_SIZE_BOT_POPULATION):
    world.objects.append(Bot(dna=DEFAULT_DNA, color=DEFAULT_COLOR))

for i in range(FOOD_LIMIT):
    world.objects.append(Food(health=randint(10, 100)))

while RUN:
    pygame.time.delay(100)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            RUN = False
    window.fill((55, 55, 55))
    for e in world.objects:
        pygame.draw.rect(window, e.color, (e.pos[0], e.pos[1], 10, 10))
        e.activate()

    clear_dead()
    food_respawn()

    pygame.display.update()

from random import randint

from const import WORLD_SIZE
from entities import Entity

class Bot(Entity):
    def __init__(self, dna, color):
        self.color = color
        self.alive = True
        self.health = 100
        self.energy = 0
        self.pos = [randint(0, WORLD_SIZE[0]), randint(0, WORLD_SIZE[1])]
        self.type = 'Bot'
        self.dna = dna

VERSION = '0.1-alpha'
WORLD_SIZE = (1280, 720)
RUN = True
STEP_SIZE = 10
START_SIZE_BOT_POPULATION = int(input("How many bots will be in the beginning: "))
FOOD_LIMIT = int(input("The maximum amount of food in the world: "))
DEFAULT_COLOR = (0, 255, 0)
FOOD_COLOR = (100, 47, 27)
DEFAULT_DNA = [10, 25, 10, 25, 10, 25, \
               10, 25, 10, 25, 10, 25, \
               10, 25, 10, 25, 10, 25, \
               10, 25, 10, 25, 10, 25, \
               10, 25, 10, 25, 10, 25, \
               10, 25]

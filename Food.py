from random import randint

from const import FOOD_COLOR, WORLD_SIZE
from entities import Entity


class Food(Entity):
    def __init__(self, health = 100):
        self.color = FOOD_COLOR  # Red
        self.alive = True
        self.health = health
        self.energy = 10
        self.pos = [randint(0, WORLD_SIZE[0]), randint(0, WORLD_SIZE[1])]
        self.type = 'Food'
        self.dna = False